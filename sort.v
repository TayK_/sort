Require Import List Arith.

Inductive sorted : list nat -> Prop :=
  snil : sorted nil
  | ssingle : forall x, sorted (x::nil)
  | scons : forall x y l, x <= y -> sorted (y :: l) -> sorted (x::y::l).

Fixpoint insert x l :=
  match l with nil => x::nil | y::l' => if leb x y then x::l else y::insert x l' end.

Lemma insert_sorted : forall x l, sorted l -> sorted (insert x l).
Proof.